<?php

function bpc_client_factory() {
	global $bpc;
	if(is_object($bpc)) {
		return $bpc;
	}
	global $current_user;
	get_currentuserinfo();

	/* Set config parameters here */
	$forening = '';
	$key = '';
	$is_testing = TRUE;

	if(is_user_logged_in()) {
		$user_username = $current_user->user_login;
		$user_name = $current_user->user_firstname . ' ' . $current_user->user_lastname;
	} else {
		$user_username = 'anonymous';
		$user_name = 'Anonymous';
	}
	/* Set how extra data is fetched here, e.g. get_user_meta() or similar */
	$user_card = '';
	$user_year = '';

	$config = new \BPC\Config($forening, $key, $is_testing);
	$connection = new \BPC\WPConnection($config);
	$user = new \BPC\User($user_username, $user_name, $user_card, $user_year);
	$bpc = new \BPC\Client($connection, $user);

	return $bpc;
}

/**
 * bpc_quarantine_status
 *
 * Check whether the user has been absent from enough presentations to qualify for quarantine
 *
 * @param \BPC\Client $bpc
 * @return bool Whether the user is quarantined
 */
function bpc_quarantine_status(\BPC\Client $bpc) {
	if(is_user_logged_in()) {
		/* First, check one month back */
		$attended = $bpc->getAttendanceStatistics('-2 months');
		if($attended !== FALSE) {
			$quarantined = $attended['not_attended'] >= 3;
		} else {
			$quarantined = FALSE;
		}

		return $quarantined; 
	}
	return TRUE;
}
