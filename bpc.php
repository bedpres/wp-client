<?php
/**
 * Plugin Name: BPC Client
 * Plugin URI: https://www.bedriftspresentasjon.no/client
 * Description: A brief description of the plugin.
 * Version: 0.1.0
 * Author: BPC
 * Author URI: https://www.bedriftspresentasjon.no
 * License: MIT
 */

include('client/autoload.php');
include('config.php');

add_shortcode('bedpres_list', 'bpc_list_events');
add_shortcode('bedpres', 'bpc_single_event');
add_action('wp_enqueue_scripts', 'bpc_scripts');
add_action('init', 'bpc_parse_postdata');
add_action('widgets_init', function() {
	register_widget('BPC_Widget');
});

$bpc_feedback = array();
$bpc_errors = array();

//[bedpres_list]
function bpc_list_events($atts) {
	global $bpc_feedback;
	global $bpc_errors;
	ob_start();
	$bpc = bpc_client_factory();
	$logged_in = is_user_logged_in();

	$events = $bpc->getEvents();
	if($events === FALSE) {
		// Error
		echo '<div class="bpc-list-events">Ingen bedpreser tilgjengelig</div>';
	} elseif (!empty($events)) {
		$extra = array(
			'logged_in' => $logged_in,
			'quarantined' => bpc_quarantine_status($bpc)
		);

		if(!empty($bpc_feedback)) {
			echo '<div class="bpc-feedback">';
			foreach($bpc_feedback as $msg) {
				echo $msg . '<br />';
			}
			echo '</div>';
		}
		if(!empty($bpc_errors)) {
			echo '<div class="bpc-error">';
			foreach($bpc_errors as $msg) {
				echo $msg . '<br />';
			}
			echo '</div>';
		}
		echo '<div class="bpc-list-events">';
		foreach($events as $event)
		{
			$event['registration_allowed'] = ($bpc->user->year >= $event['min_year']) && ($bpc->user->year <= $event['max_year']);
			bpc_render_event($event, $extra);
		}
		echo '</div>';
	} else {
		// No events
		echo '<div class="bpc-list-events">Ingen bedpreser tilgjengelig</div>';
	}

	$ret = ob_get_clean();

	return $ret;
}

// [bedpres id=foo]
function bpc_single_event($atts) {
	$a = shortcode_atts( array(
			'id' => 0
	), $atts );

	global $bpc_feedback;
	global $bpc_errors;
	$bpc = bpc_client_factory();
	ob_start();

	$event = $bpc->getEvent($a['id']);
	if($event === FALSE) {
		// Error
		echo '<div class="bpc-list-events">Ingen bedpreser tilgjengelig</div>';
	} elseif(!empty($event)) {
		$extra = array(
				'logged_in' => is_user_logged_in(),
				'quarantined' => bpc_quarantine_status($bpc)
		);
		$event['registration_allowed'] = ($bpc->user->year >= $event['min_year']) && ($bpc->user->year <= $event['max_year']);

		if(!empty($bpc_feedback)) {
			echo '<div class="bpc-feedback">';
			foreach($bpc_feedback as $msg) {
				echo $msg . '<br />';
			}
			echo '</div>';
		}
		if(!empty($bpc_errors)) {
			echo '<div class="bpc-error">';
			foreach($bpc_errors as $msg) {
				echo $msg . '<br />';
			}
			echo '</div>';
		}

		echo '<div class="bpc-list-events">';
		bpc_render_event($event, $extra);
		echo '</div>';
	} else {
		// Error
		echo '<div class="bpc-list-events">Ingen bedpreser tilgjengelig</div>';
	}
	return ob_get_clean();
}

function bpc_render_event($event, $extra) {
	global $bpc_feedback;
	global $bpc_errors;
	extract($event);
	extract($extra);
	if($event['is_waiting']) {
		$num_waiting = bpc_get_num_waiting($event['id']);
	}
	include('templates/event-single.php');
}

function bpc_get_num_waiting($eid) {
	$bpc = bpc_client_factory();
	$waiting = $bpc->getWaitingUsers($eid);
	$num_waiting = 1;
	if(!empty($waiting)) {
		foreach($waiting as $person) {
			if($person['username'] == $bpc->user->username) {
				break;
			}
			$num_waiting++;
		}
	}
	return $num_waiting;
}

function bpc_scripts() {
	wp_register_style('bpc_style', plugins_url('templates/style.css', __FILE__));
	wp_enqueue_style('bpc_style');
}

function bpc_parse_postdata() {
	global $bpc_feedback;
	global $bpc_errors;
	if(isset($_POST['bpc_register'])) {
		$eid = $_POST['bpc_event_id'];
		if($eid) {
			$bpc = bpc_client_factory();
			$res = $bpc->registerUser($eid);
			if(!$res) {
				$err = $bpc->getLastError();
			} else {
				if($res === TRUE) {
					$bpc_feedback[] = "Du er nå påmeldt.";
				} elseif(isset($res['waiting'])) {
					$bpc_feedback[] = "Du står som nummer {$res['waiting']} på venteliste.";
				}
			}
		}
	}
	if(isset($_POST['bpc_deregister'])) {
		$eid = $_POST['bpc_event_id'];
		if($eid) {
			$bpc = bpc_client_factory();
			$res = $bpc->deregisterUser($eid);
			if(!$res) {
				$err = $bpc->getLastError();
				// Handle error
			} else {
				$bpc_feedback[] = "Du er nå avmeldt.";
			}
		}
	}

	if(isset($err)) {
		// Handle error
		foreach($err as $code => $msg) {
			$message = NULL;
			switch($code) {
				case 105:
					$message = "Brukernavn er ugyldig, kontakt administrator.";
					break;
				case 106:
					$message = "Du må oppgi fullt navn i profilen for å kunne melde deg på.";
					break;
				case 107:
					$message = "Du må registrere NTNU-kortnummer i profilen for å kunne melde deg på.";
					break;
				case 401:
				case 410:
					$message = "Du har ikke tilgang til denne bedpresen";
					break;
				case 402:
					$message = "Beklager, bedpresen er full.";
					break;
				case 405:
					$message = "Du er allerede påmeldt!";
					break;
				case 406:
					$message = "Du er ikke påmeldt, og kan derfor ikke melde deg av.";
					break;
				case 407:
					$message = "Du har blitt påmeldt gjennom en annen linjeforening, og kan derfor ikke melde deg av her.";
					break;
				case 408:
					$message = "Påmeldingen har ikke startet ennå!";
					break;
				case 409:
					$message = "Påmeldingen er avsluttet, du kan ikke melde deg på nå.";
					break;
				default:
					$message = "Kunne ikke utføre operasjonen.";
					break;
			}
			$bpc_errors[] = $message;
		}
	}
}

class BPC_Widget extends WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
				'bpc_widget', // Base ID
				__( 'Bedpresliste', 'text_domain' ), // Name
				array( 'description' => __( 'En liste over kommende bedpreser', 'text_domain' ), ) // Args
		);
	}

	public function widget($args, $instance) {
		echo $args['before_widget'];
		$elems = (!empty($instance['elements'])) ? $instance['elements'] : 4;
		$bpc = bpc_client_factory();
		$events = $bpc->getEvents();
		if(empty($events)) {
			$events = array();
		}
		$events = array_slice($events, 0, $elems, TRUE);
		include('templates/widget.php');
		echo $args['after_widget'];	}

	public function update($new_instance, $old_instance) {
		$inst = array();
		$inst['elements'] = !empty($new_instance['elements']) ? $new_instance['elements'] : 4;
		return $inst;
	}

	public function form($instance) {
		$elems = ! empty( $instance['elements'] ) ? $instance['elements'] : 4;
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'elements' ); ?>">Antall bedpreser:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'elements' ); ?>" name="<?php echo $this->get_field_name( 'elements' ); ?>" type="text" value="<?php echo esc_attr( $elems ); ?>">
		</p>
	<?php 	}
}
