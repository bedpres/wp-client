<?php
// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
?>
<article class="bpc-event">
	<header>
		<a href="<?= $web_page ?>" style="background: url('<?= $logo ?>') no-repeat center center;"></a>
		<?= $title ?>
	</header>
	<aside>
		<table>
			<tr>
				<td><strong>Sted:</strong></td>
				<td><?= $place ?></td>
			</tr>
			<tr>
				<td><strong>Tidspunkt:</strong></td>
				<td><?= date('d. F Y H:i',strtotime($time)) ?></td>
			</tr>
			<tr>
				<td><strong>Åpen for:</strong></td>
				<td><?= $min_year ?>. <?php if($max_year != $min_year) echo ($max_year >= 5) ? "- 5" : "- ".$max_year; ?>. klasse</td>
			</tr>
			<tr>
				<td><strong>Påmeldt:</strong></td>
				<td><?= $seats - $seats_available ?>/<?= $seats ?><?php if($count_waiting) echo ", $count_waiting på venteliste"; ?></td>
			</tr>
		</table>
		<?php if($registration_started): ?>
			<?php if(!$deadline_passed): ?>
				<?php if(!$logged_in): ?>
					<em>Du er ikke innlogget. Logg inn for å melde deg på.</em>
				<?php elseif($quarantined): ?>
					<em>Du har for mange prikker, og er satt i karantene.</em>
				<?php elseif(!$registration_allowed): ?>
					<em>Du har ikke tilgang til denne bedpresen.</em>
				<?php else: ?>
						<?php if($attending): ?>
							<form method="POST" action="">
								<input type="hidden" name="bpc_event_id" value="<?= $id ?>" />
								<input type="submit" name="bpc_deregister" value="Meld meg av" />
							</form>
						<?php elseif($is_waiting): ?>
							<em>Du er påmeldt, og står som nummer <?= $num_waiting ?> på ventelista.</em>
							<form method="POST" action="">
								<input type="hidden" name="bpc_event_id" value="<?= $id ?>" />
								<input type="submit" name="bpc_deregister" value="Fjern meg fra ventelista" />
							</form>
						<?php elseif($seats_available > 0): ?>
							<form method="POST" action="">
								<input type="hidden" name="bpc_event_id" value="<?= $id ?>" />
								<input type="submit" name="bpc_register" value="Meld meg på" />
							</form>
						<?php elseif($waitlist_enabled): ?>
							<form method="POST" action="">
								<input type="hidden" name="bpc_event_id" value="<?= $id ?>" />
								<input type="submit" name="bpc_register" value="Sett meg på venteliste" />
							</form>
						<?php else: ?>
							<em>Bedpresen er fulltegnet.</em>
						<?php endif; ?>
				<?php endif; ?>
			<?php else: ?>
				<em>Påmeldingen er avsluttet. Du er <?php
					if($attending)
						echo "påmeldt";
					elseif($is_waiting)
						echo "nummer $num_waiting på venteliste";
					else
						echo "ikke påmeldt";
					?>.</em>
			<?php endif; ?>
		<?php else: ?>
		  <em>Påmeldingen starter <?= date('d. F Y H:i',strtotime($registration_start)) ?>.</em>
		<?php endif; ?>
	</aside>

	<div class="bpc-description">
		<?= $description_formatted ?>
	</div>
</article>
