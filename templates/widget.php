<?php
// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
?>
<h2 class="widget-title">Kommende bedpreser</h2>
<ul>
	<?php foreach($events as $event): ?>
		<li><?= $event['title'] ?> (<?= date('d. M', strtotime($event['time'])) ?>)</li>
	<?php endforeach; ?>
	<?php if(empty($events)): ?>
		<li style="font-style: italic;">Ingen kommende bedpreser</li>
	<?php endif; ?>
</ul>
