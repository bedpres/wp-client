# WordPress client for BPC

This WordPress client for BPC exposes two shortcodes:

* `[bedpres_list]` is a list of all presentations in the future
* `[bedpres id=<id>]`shows a single presentation

## Setup

Checkout the repository into your wordpress installation:

    git clone https://bitbucket.org/bedpres/wp-client.git wp-content/plugins/bpc

Initialize the client library (shipped as a git submodule):

    cd wp-content/plugins/bpc
    git submodule init
    git submodule update

Activate it on the Plugins page.

Edit the following items in `config.php`:

- Add your credentials in `$forening`  and `$key`
- Populate the `$user_*` variables using data from your user profiles. This means that this information needs to be stored separately (e.g. somewhere where `get_user_meta()` can find it). This can be done using plugins like Cimy User Extra Fields or Pie Register.
- Change the `bpc_quarantine_status()` function according to your policy for excluding people who have a tendency to not show up to presentations (e.g. 3 no-shows in the last 3 months).